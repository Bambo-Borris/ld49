#pragma once

#include <Krawler.h>

namespace GConsts
{
	// Window Constants
	constexpr Krawler::uint32 TARGET_WIDTH = 640;  // Used for setting view size
	constexpr Krawler::uint32 TARGET_HEIGHT = 480; // Used for setting view size
	constexpr const char *WINDOW_TITLE = "Reactor Core <UnStAbLe> (LD49 Entry)";

	// Scene Names
	constexpr const char *GAMEPLAY_SCENE_NAME = "GameplayScene";
	constexpr const char *MENU_SCENE_NAME = "MenuScene";

	// Entity Tags, these will be used a lot
	constexpr const char *GAME_MANAGER_TAG = "GameManagerEntity";
	constexpr const char *REACTOR_CORE_TAG = "ReactorCoreEntity";
	constexpr const char *REACTOR_TEMPERATURE_TAG = "ReactorTemperatureEntity";
	constexpr const char *REACTOR_POWER_TAG = "ReactorPowerEntity";
	constexpr const char *COOLANT_TEMP_TAG = "CoolantTemperatureEntity";
	constexpr const char *FUEL_ROD_TAG = "FuelRodEntity";
	constexpr const char *OVERLAY_TAG = "OverlayEntity";
	constexpr const char *LOWER_BOUND_TAG = "LowerBoundEntity";
	constexpr const char *UPPER_BOUND_TAG = "UpperBoundEntity";

	// Entity Sizes
	const inline Krawler::Vec2f REACTOR_CORE_SIZE = Krawler::Vec2f{340.0f, 310.0f};
	const inline Krawler::Vec2f REACTOR_LONG_PANEL_SIZE = Krawler::Vec2f{80.0f, 375.0f};
	const inline Krawler::Vec2f COOLANT_PANEL_SIZE = Krawler::Vec2f{340.0f, 50.0f};
	const inline Krawler::Vec2f CONTROL_ROD_MAX_SIZE = Krawler::Vec2f{15.0f, 310.0f};
	const inline Krawler::Vec2f LONG_PANE_OUTLINE = Krawler::Vec2f(4.0f, 4.0f);
	const inline Krawler::Vec2f FUEL_ROD_SIZE = Krawler::Vec2f(30.0f, 160.0f);
	const inline Krawler::Vec2f CONTROL_ROD_SIZE = Krawler::Vec2f(15.0f, 310.0f);
	const inline Krawler::Vec2f PARTICLE_SIZE = Krawler::Vec2f(5.0f, 5.0f);


	// Entity Locations
	const inline Krawler::Vec2f REACTOR_CORE_POSITION = Krawler::Vec2f{262.0f, 30.0f};
	const inline Krawler::Vec2f REACTOR_TEMPERATURE_POSITION = Krawler::Vec2f{20.0f, 30.0f};
	const inline Krawler::Vec2f REACTOR_POWER_POSITION = Krawler::Vec2f{122.0f, 30.0f};
	const inline Krawler::Vec2f LONG_PANE_FILL_OFFSET = Krawler::Vec2f(2.0f, 2.0f);

	// Gameplay Constants
	constexpr float REACTOR_MAX_TEMP = 600.0f;	// Celcius
	constexpr float REACTOR_MAX_POWER = 700.0f; // Megawatts
	constexpr float ROD_INSERTION_SPEED = 90.0f;
	constexpr Krawler::uint64 FUEL_ROD_COUNT = 4;
	constexpr Krawler::uint64 CONTROL_ROD_COUNT = 3;
	constexpr Krawler::uint64 REACTOR_PARTICLE_COUNT = 50;
}