#pragma once

#include <SFML\Graphics\RectangleShape.hpp>
#include <SFML\Audio\Sound.hpp>

class ControlRod : public Krawler::KComponentBase
{
public:
	ControlRod(Krawler::KEntity *entity);
	~ControlRod() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void tick() override;

	float getInsertionDepthPercentage() const { return m_insertionDepthPercentage; }
	void resetControlRod();

private:
	Krawler::Components::KCSprite &m_sprite;
	float m_insertionDepthPercentage = 50.0f;
	bool m_isEmergencyInsert = false;
	sf::Sound m_emergencyInsertSound;
};