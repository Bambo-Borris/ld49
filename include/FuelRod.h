#pragma once

class FuelRod : public Krawler::KComponentBase
{
public:
	FuelRod(Krawler::KEntity *entity);
	~FuelRod() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void tick() override;

private:
	const Krawler::Vec3f START_COL = Krawler::Vec3f(43.0f / 255.0f, 250.0f / 255.0f, 28.0f / 255.0f);
	const Krawler::Vec3f END_COL = Krawler::Vec3f(225.0f / 255.0f, 249.0f / 255.0f, 87.0f / 255.0f);

	Krawler::Components::KCSprite &m_sprite;
	Krawler::Vec3f m_currentCol;
	float m_lerpT = 0.0f;
	bool m_lerpInverted = false;
};