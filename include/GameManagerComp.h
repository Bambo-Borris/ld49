#pragma once

#include "Constants.h"

#include <SFML\Audio\Sound.hpp>

class ReactorCore;
class ReactorTemperature;
class ReactorPower;
class ControlRod;
class GameOverlay;

class GameManagerComp : public Krawler::KComponentBase
{
public:
	GameManagerComp(Krawler::KEntity *entity);
	~GameManagerComp() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void tick() override;
	virtual void onEnterScene() override;

	bool isGameActive() const { return m_isLevelActive; }

private:
	void resetGame();
	void pickNewBounds();
	std::string getSurvivalTimeString() const;

	std::array<ControlRod *, GConsts::CONTROL_ROD_COUNT> m_controlRods;
	ReactorCore *m_reactorCore = nullptr;
	ReactorTemperature *m_reactorTemp = nullptr;
	ReactorPower *m_reactorPower = nullptr;
	GameOverlay *m_overlay = nullptr;

	Krawler::KEntity *m_lowerBound = nullptr;
	Krawler::KEntity *m_upperBound = nullptr;

	// Min & Max y positions the  lower/upper bound
	// arrows can be positioned at
	float m_maxBoundYPos = 0.0f;
	float m_minBoundYPos = 0.0f;
	float m_boundsDifferencePercent = 0.2f;
	float m_survivalTimer = 3.5f;

	sf::Clock m_gamerTimer;		// Timer for score
	sf::Clock m_challengeTimer; // Timer for current score
	sf::Clock m_outsideRangeTimer;

	sf::Sound m_failSirenSound;
	sf::Sound m_successSound;
	sf::Sound m_outsideRangeSound;

	bool m_isLevelActive = false;
	bool m_hasBecomeWithinRange = false;
	Krawler::uint64 m_levelAttemptCount = 0;
};