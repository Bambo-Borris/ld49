#pragma once

#include <SFML\Graphics\RectangleShape.hpp>
#include <SFML\Graphics\Text.hpp>

class GameOverlay : public Krawler::Components::KCRenderableBase
{
public:
	GameOverlay(Krawler::KEntity *entity);
	~GameOverlay() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	virtual Krawler::Rectf getOnscreenBounds() const { return {}; }

	void setOverlayString(std::string_view str, bool isGameOver = false);

private:
	sf::Text m_overlayText;
	sf::RectangleShape m_overlayBackground;
};