#pragma once

#include <SFML\Graphics\RectangleShape.hpp>
#include <SFML\Graphics\Text.hpp>

class LongFillPanel : public Krawler::Components::KCRenderableBase
{
public:
	LongFillPanel(Krawler::KEntity *entity, std::string_view panelText);
	~LongFillPanel() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void onEnterScene() override;
	virtual void tick() override;

	virtual Krawler::Rectf getOnscreenBounds() const { return {}; }
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	float getFillPercentage() const { return m_fillPercent; }
	void setFillPercentage(float percent) { m_fillPercent = percent; }

	void setFillColour(const sf::Color &c) { m_fill.setFillColor(c); }
	float getFillYPosition() const;

private:
	void updateFill();

	Krawler::Components::KCSprite &m_sprite;
	sf::RectangleShape m_fill;
	sf::Text m_panelText;
	std::string m_panelString;

	float m_fillPercent = 0.0f;
};