#pragma once

#include "Constants.h"

class ReactorParticles : public Krawler::Components::KCRenderableBase
{
public:
	ReactorParticles(Krawler::KEntity *entity);
	~ReactorParticles() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void onEnterScene() override;
	virtual void tick() override;

	virtual Krawler::Rectf getOnscreenBounds() const { return {}; }
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	void resetParticles();

private:
	sf::VertexArray m_particles;
	std::array<Krawler::Rectf, GConsts::FUEL_ROD_COUNT> m_fuelRods;
	std::array<Krawler::KEntity *, GConsts::CONTROL_ROD_COUNT> m_controlRods;
	std::array<Krawler::Vec2f, GConsts::REACTOR_PARTICLE_COUNT> m_velocities;
	sf::Texture* m_texture;

	void handleParticleCollisions(const Krawler::Rectf& object, const Krawler::Rectf& particle, Krawler::uint64 idx);
};