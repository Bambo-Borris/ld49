#pragma once

#include "LongFillPanel.h"

class ReactorPower : public LongFillPanel
{
public:
	ReactorPower(Krawler::KEntity *entity);
	~ReactorPower() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void tick() override;

private:
};