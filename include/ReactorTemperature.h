#pragma once

#include "LongFillPanel.h"

class ReactorTemperature : public LongFillPanel
{
public:
	ReactorTemperature(Krawler::KEntity *entity);
	~ReactorTemperature() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void tick() override;
private:
};