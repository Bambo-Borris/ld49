#pragma once

class SetupGameComp : public Krawler::KComponentBase
{
public:
	SetupGameComp(Krawler::KEntity *entity);
	~SetupGameComp() = default;

	virtual Krawler::KInitStatus init() override;

private:
};