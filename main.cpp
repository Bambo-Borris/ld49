#include "stdafx.h"
#include "Constants.h"
#include "SetupGameComp.h"
#include "MenuSceneManager.h"

extern "C"
{
	_declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;
	__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}

using namespace Krawler;

#ifndef _DEBUG
#include <Windows.h>
int WINAPI WinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance, LPSTR lpszArgument, int nCmdShow)
#else
int main(void)
#endif
{
	const auto &fullscreenModes = sf::VideoMode::getFullscreenModes();

	KApplicationInitialise initApp(false);
	initApp.gameFps = 60;
	initApp.physicsFps = 60;
	initApp.width = fullscreenModes[0].width;
	initApp.height = fullscreenModes[0].height;
	initApp.windowStyle = KWindowStyle::Fullscreen;

	// initApp.width = 1024;
	// initApp.height = 768;
	// initApp.windowStyle = KWindowStyle::Windowed_Resizeable;

	initApp.windowTitle = GConsts::WINDOW_TITLE;

	StartupEngine(&initApp);

	GET_DIRECTOR().addScene(new KScene(GConsts::GAMEPLAY_SCENE_NAME, Rectf(0, 0, (70 * 32), (40 * 32))));
	GET_DIRECTOR().addScene(new KScene(GConsts::MENU_SCENE_NAME, Rectf(0, 0, (70 * 32), (40 * 32))));

	//app->getSceneDirector().addScene(new KScene("Menu_Scene", Rectf(0, 0, (70 * 32), (40 * 32))));
	GET_DIRECTOR().setStartScene(GConsts::MENU_SCENE_NAME);

	KScene *gameplayScene = GET_DIRECTOR().getSceneByName(GConsts::GAMEPLAY_SCENE_NAME);
	KScene *menuScene = GET_DIRECTOR().getSceneByName(GConsts::MENU_SCENE_NAME);

	KCHECK(gameplayScene);
	KCHECK(menuScene);

	auto &gameplaySceneSetupEntity = *gameplayScene->addEntityToScene();
	gameplaySceneSetupEntity.addComponent(new SetupGameComp(&gameplaySceneSetupEntity));

	auto &menuSceneSetupEntity = *menuScene->addEntityToScene();
	menuSceneSetupEntity.addComponent(new MenuSceneManager(&menuSceneSetupEntity));

	if (InitialiseSubmodules() != KInitStatus::Success)
	{
		return 1;
	}
	RunApplication();
	ShutdownEngine();

	return 0;
}
