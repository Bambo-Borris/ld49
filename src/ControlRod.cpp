#include "stdafx.h"
#include "ControlRod.h"
#include "Constants.h"
#include "GameManagerComp.h"

using namespace Krawler;
using namespace Krawler::Components;
using namespace Krawler::Input;

ControlRod::ControlRod(KEntity *entity)
	: KComponentBase(entity), m_sprite(*new KCSprite(entity))
{
	getEntity()->addComponent(&m_sprite);
}

KInitStatus ControlRod::init()
{
	m_sprite.setSize(GConsts::CONTROL_ROD_SIZE);
	m_sprite.setColour({0, 162, 232});
	m_sprite.setRenderLayer(2);
	
	auto insertSound = ASSET().getSound("emergency_insert");
	KCHECK(insertSound);
	m_emergencyInsertSound = sf::Sound(*insertSound);
	m_emergencyInsertSound.setLoop(true);

	return KInitStatus::Success;
}

void ControlRod::tick()
{
	auto gmEntity = GET_SCENE()->findEntity(GConsts::GAME_MANAGER_TAG);
	KCHECK(gmEntity);
	if (auto gm = gmEntity->getComponent<GameManagerComp>())
	{
		if (!gm->isGameActive())
		{
			return;
		}
	}
	const float dt = GET_APP()->getDeltaTime();

	if (KInput::Pressed(KKey::S) || KInput::Pressed(KKey::Down))
	{
		if (!m_isEmergencyInsert)
		{
			m_insertionDepthPercentage += GConsts::ROD_INSERTION_SPEED * dt;
		}
	}

	if (KInput::Pressed(KKey::W) || KInput::Pressed(KKey::Up))
	{
		if (!m_isEmergencyInsert)
		{
			m_insertionDepthPercentage -= GConsts::ROD_INSERTION_SPEED * dt;
		}
	}

	if (KInput::JustPressed(KKey::Space))
	{
		if (!m_isEmergencyInsert)
		{
			m_isEmergencyInsert = true;
			m_emergencyInsertSound.play();
		}
	}

	if (m_isEmergencyInsert)
	{
		m_insertionDepthPercentage += GConsts::ROD_INSERTION_SPEED * dt;
	}

	m_insertionDepthPercentage = Maths::Clamp(0.0f, 100.0f, m_insertionDepthPercentage);

	if (m_isEmergencyInsert && m_insertionDepthPercentage == 100.0f)
	{
		m_isEmergencyInsert = false;
		m_emergencyInsertSound.stop();
	}

	float newHeight = (m_insertionDepthPercentage / 100.0f) * GConsts::CONTROL_ROD_SIZE.y;
	m_sprite.setSize(Vec2f{GConsts::CONTROL_ROD_SIZE.x, newHeight});
}

void ControlRod::resetControlRod()
{
	m_insertionDepthPercentage = 50.0f;
}
