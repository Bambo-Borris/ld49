#include "stdafx.h"
#include "FuelRod.h"
#include "Constants.h"

using namespace Krawler;
using namespace Krawler::Components;

FuelRod::FuelRod(Krawler::KEntity *entity)
	: KComponentBase(entity), m_sprite(*new KCSprite(entity, GConsts::FUEL_ROD_SIZE))
{
	getEntity()->addComponent(&m_sprite);
}

KInitStatus FuelRod::init()
{
	m_currentCol = START_COL;
	m_sprite.setColour(sf::Color(KCAST(uint8, m_currentCol.x * 255.0f), KCAST(uint8, m_currentCol.y * 255.0f), KCAST(uint8, m_currentCol.z * 255.0f)));
	m_lerpT = Maths::RandFloat(0.0f, 1.0f);
	m_sprite.setRenderLayer(2);
	return KInitStatus::Success;
}

void FuelRod::tick()
{
	const float dt = GET_APP()->getDeltaTime();
	m_currentCol.x = Maths::Lerp(START_COL.x, END_COL.x, m_lerpT);
	m_currentCol.y = Maths::Lerp(START_COL.y, END_COL.y, m_lerpT);
	m_currentCol.z = Maths::Lerp(START_COL.z, END_COL.z, m_lerpT);

	m_sprite.setColour({KCAST(uint8, m_currentCol.x * 255.0f), KCAST(uint8, m_currentCol.y * 255.0f), KCAST(uint8, m_currentCol.z * 255.0f)});

	!m_lerpInverted ? m_lerpT += dt : m_lerpT -= dt;
	if (m_lerpT >= 1.0f)
	{
		m_lerpInverted = true;
	}
	else if (m_lerpT <= 0.0f)
	{
		m_lerpInverted = false;
	}
}
