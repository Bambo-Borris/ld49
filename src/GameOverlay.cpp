#include "stdafx.h"
#include "GameOverlay.h"
#include "Constants.h"

using namespace Krawler;

GameOverlay::GameOverlay(Krawler::KEntity *entity)
	: Components::KCRenderableBase(entity)
{
}

KInitStatus GameOverlay::init()
{
	auto font = ASSET().getFont("vcr_osd_mono");
	m_overlayText.setFont(*font);
	m_overlayBackground.setSize({KCAST(float, GConsts::TARGET_WIDTH), KCAST(float, GConsts::TARGET_HEIGHT)});
	m_overlayBackground.setFillColor({50, 50, 50, 225});

	setRenderLayer(100);
	return KInitStatus::Success;
}

void GameOverlay::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
	target.draw(m_overlayBackground);
	target.draw(m_overlayText);
}

void GameOverlay::setOverlayString(std::string_view str, bool isGameOver)
{
	m_overlayText.setString(str.data());
	Vec2f centre = m_overlayBackground.getSize() * 0.5f;
	Vec2f newPosition = centre - (Vec2f(m_overlayText.getGlobalBounds().width, m_overlayText.getGlobalBounds().height) * 0.5f);
	m_overlayText.setPosition(newPosition);
	if (isGameOver)
	{
		m_overlayText.setFillColor(sf::Color::Red);
	}
	else
	{
		m_overlayText.setFillColor(sf::Color::White);
	}
}
