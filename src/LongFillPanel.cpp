#include "stdafx.h"
#include "LongFillPanel.h"

#include "stdafx.h"
#include "ReactorTemperature.h"
#include "Constants.h"

#include <Components\KCSprite.h>

using namespace Krawler;
using namespace Krawler::Components;
using namespace Krawler::Input;

LongFillPanel::LongFillPanel(KEntity *entity, std::string_view panelText)
	: KCRenderableBase(entity), m_sprite(*new KCSprite(entity)), m_panelString(panelText)
{
}

KInitStatus LongFillPanel::init()
{
	m_sprite.setSize(GConsts::REACTOR_LONG_PANEL_SIZE);
	// m_sprite.setColour(sf::Color::Red);
	getEntity()->addComponent(&m_sprite);
	auto tex = ASSET().getTexture("reactor_long_panel");
	m_sprite.setTexture(tex);
	m_sprite.setRenderLayer(0);
	setRenderLayer(1);

	auto font = ASSET().getFont("vcr_osd_mono");
	m_panelText.setFont(*font);
	m_panelText.setString(m_panelString);
	auto bounds = m_panelText.getGlobalBounds();
	m_panelText.setOrigin(Vec2f{bounds.width, bounds.height} * 0.5f);
	m_panelText.setRotation(-90.0f);
	m_panelText.setFillColor(sf::Color::White);
	updateFill();

	return KInitStatus::Success;
}

void LongFillPanel::onEnterScene()
{
	Vec2f pos = getEntity()->m_pTransform->getPosition() + (GConsts::REACTOR_LONG_PANEL_SIZE * 0.5f);
	pos.x -= 5.0f;
	m_panelText.setPosition(pos);
}

void LongFillPanel::tick()
{
	m_fillPercent = Maths::Clamp(0.0f, 100.0f, m_fillPercent);
	updateFill();
}

void LongFillPanel::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
	target.draw(m_fill);
	target.draw(m_panelText);
}

void LongFillPanel::updateFill()
{
	const Vec2f MaxFillSize = GConsts::REACTOR_LONG_PANEL_SIZE - GConsts::LONG_PANE_OUTLINE;
	Vec2f origin = getEntity()->m_pTransform->getPosition() + GConsts::LONG_PANE_FILL_OFFSET;
	const float NewFilLSize = (MaxFillSize.y * (m_fillPercent / 100.0f));
	// We need to calculate where the fill rectangle should be
	// so we figure out where it would be at a full position
	// then add on the difference between its max size & the size it will be
	// at this percentage of fill. We only do it for y since we want the x position
	// to always be the same.
	origin.y = origin.y + (MaxFillSize.y - NewFilLSize);
	m_fill.setPosition(origin);
	m_fill.setSize({MaxFillSize.x, NewFilLSize});
}

float LongFillPanel::getFillYPosition() const
{
	return m_fill.getPosition().y;
}