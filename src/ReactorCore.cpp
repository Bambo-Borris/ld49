#include "stdafx.h"
#include "ReactorCore.h"
#include "ImguiComp.h"
#include "ControlRod.h"
#include "GameManagerComp.h"

using namespace Krawler;
using namespace Krawler::Components;

ReactorCore::ReactorCore(KEntity *entity)
	: KComponentBase(entity), m_sprite(*new KCSprite(entity))
{
}

KInitStatus ReactorCore::init()
{
	getEntity()->setTag(GConsts::REACTOR_CORE_TAG);
	getEntity()->addComponent(&m_sprite);

	m_sprite.setSize(GConsts::REACTOR_CORE_SIZE);
	// m_sprite.setColour(sf::Color{55, 55, 55});
	auto tex = ASSET().getTexture("reactor_core_pane");
	m_sprite.setTexture(tex);

	getEntity()->m_pTransform->setPosition(GConsts::REACTOR_CORE_POSITION);

	return KInitStatus::Success;
}

void ReactorCore::onEnterScene()
{
	auto entityList = GET_SCENE()->getAllocatedEntityList();
	uint64 crIdx = 0;
	for (auto &e : entityList)
	{
		if (auto comp = e->getComponent<ControlRod>())
		{
			m_controlRods[crIdx] = comp;
			++crIdx;
		}

		if (crIdx >= m_controlRods.size())
		{
			break;
		}
	}

	resetCore();
}

void ReactorCore::tick()
{
	auto gameManager = GET_SCENE()->findEntity(GConsts::GAME_MANAGER_TAG);
	KCHECK(gameManager);
	if (auto gm = gameManager->getComponent<GameManagerComp>())
	{
		// Do nothing until the user inputs a key
		// indicating they're ready to start the game
		if (!gm->isGameActive())
		{
			return;
		}
	}

	auto e = GET_SCENE()->findEntity(GConsts::GAME_MANAGER_TAG);
	auto imguiComp = e->getComponent<ImguiComp>();
	imguiComp->update();
	imguiComp->begin("Reactor Core Vars");

	ImGui::Text(fmt::format("Reactor Temp = {} \nReactor Power = {}", m_coreTemperature, m_powerOutput).c_str());
	ImGui::InputFloat("Reaction Rate Constant", &m_reactionRateConst);
	ImGui::InputFloat("Temp Constant", &m_tempConst);
	ImGui::InputFloat("Power Constant", &m_powerConst);
	ImGui::InputFloat("Core Temp Decay Rate", &m_coreTempDecay);
	ImGui::InputFloat("Reaction Rate Cap", &m_rateOfReactionCap);

	if (ImGui::Button("Reset"))
	{
		m_reactionRate = 0.0f;
		m_coreTemperature = 0.0f;
		// m_powerOutput = 0.0f;
	}

	const float dt = GET_APP()->getDeltaTime();
	//ImGui::SliderFloat("Water Constant", &m_k3, 0.0f, 100.0f);
	float deltaReactionRate = (dt * m_reactionRateConst);
	/*if (deltaReactionRate < 0.01f)
	{
		deltaReactionRate = 0.0f;
	}*/
	bool areAllRodsInserted = true;
	for (auto &rod : m_controlRods)
	{
		if (rod->getInsertionDepthPercentage() != 100.0f)
		{
			areAllRodsInserted = false;
			break;
		}
	}

	if (!areAllRodsInserted)
	{
		m_reactionRate += deltaReactionRate;
		const float slice = m_reactionRate / KCAST(float, m_controlRods.size());
		for (auto &rod : m_controlRods)
		{
			if (rod->getInsertionDepthPercentage() > 0.0f)
			{
				float change = (slice * (rod->getInsertionDepthPercentage() / 100.0f));
				m_reactionRate -= change;
			}
		}

		m_coreTemperature += (m_tempConst * m_reactionRate) * dt; // /m_rodInsertionDepth

		m_coreTemperature = Maths::Min(m_coreTemperature, GConsts::REACTOR_MAX_TEMP);
		m_reactionRate = Maths::Min(m_rateOfReactionCap, m_reactionRate);
	}
	else
	{
		//Decay
		m_reactionRate -= m_reactionRateConst * dt;
		// spdlog::debug("Reaction rate {} ", m_reactionRate);
		m_reactionRate = Maths::Max(0.0f, m_reactionRate);
		if (m_reactionRate == 0.0f)
		{
			m_coreTemperature -= m_coreTempDecay * dt;
			m_coreTemperature = Maths::Max(m_coreTemperature, 0.0f);
		}
	}
	m_powerOutput = (m_powerConst * m_coreTemperature);
	imguiComp->end();
}

void ReactorCore::resetCore()
{
	m_reactionRate = 0.0f;
	m_coreTemperature = 0.0f;
	m_powerOutput = 0.0f;

	m_reactionRateConst = 150.0f;
	m_tempConst = 50.0f;
	m_powerConst = 1.37f;

	m_rateOfReactionCap = 250.0f;
	m_coreTempDecay = 90.0f;
}
