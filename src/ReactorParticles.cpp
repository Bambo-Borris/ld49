#include "stdafx.h"
#include "ReactorParticles.h"
#include "ControlRod.h"
#include "FuelRod.h"
#include <thread>

using namespace Krawler;
using namespace Krawler::Components;

ReactorParticles::ReactorParticles(KEntity *entity)
	: KCRenderableBase(entity)
{
}

KInitStatus ReactorParticles::init()
{
	m_particles.setPrimitiveType(sf::PrimitiveType::Quads);
	m_particles.resize(GConsts::REACTOR_PARTICLE_COUNT * 4);
	setRenderLayer(1);
	m_texture = ASSET().getTexture("particle");
	KCHECK(m_texture);
	for (uint64 i = 0; i < m_particles.getVertexCount(); i += 4)
	{
		sf::Vertex *v = &m_particles[i];
		// m_particles[i].color = sf::Color::Magenta;
		v[0].position = Vec2f(-100.0f, -100.0f);
		v[1].position = Vec2f(-100.0f, -100.0f);
		v[2].position = Vec2f(-100.0f, -100.0f);
		v[3].position = Vec2f(-100.0f, -100.0f);

		v[0].texCoords = Vec2f(0.0f, 0.0f);
		v[1].texCoords = Vec2f(KCAST(float, m_texture->getSize().x), 0.0f);
		v[2].texCoords = Vec2f(KCAST(float, m_texture->getSize().x), KCAST(float, m_texture->getSize().y));
		v[3].texCoords = Vec2f(0.0f, KCAST(float, m_texture->getSize().y));
	}
	return KInitStatus::Success;
}

void ReactorParticles::onEnterScene()
{
	uint64 fuelRodIdx = 0, controlRodIdx = 0;
	auto allEntities = GET_SCENE()->getAllocatedEntityList();

	for (auto &e : allEntities)
	{
		if (e->getComponent<FuelRod>())
		{
			if (fuelRodIdx < m_fuelRods.size())
			{
				Rectf rect;
				rect.left = e->m_pTransform->getPosition().x;
				rect.top = e->m_pTransform->getPosition().y;
				rect.width = GConsts::FUEL_ROD_SIZE.x;
				rect.height = GConsts::FUEL_ROD_SIZE.y;
				m_fuelRods[fuelRodIdx] = rect;
				++fuelRodIdx;
			}
		}
		else if (e->getComponent<ControlRod>())
		{
			if (controlRodIdx < m_controlRods.size())
			{
				m_controlRods[controlRodIdx] = e;
				++controlRodIdx;
			}
		}
	}

	resetParticles();
}

void ReactorParticles::tick()
{
	// Check for intersections with walls
	for (uint64 i = 0; i < m_particles.getVertexCount(); i += 4)
	{
		Rectf particle;
		particle.left = m_particles[i].position.x;
		particle.top = m_particles[i].position.y;
		particle.width = GConsts::PARTICLE_SIZE.x;
		particle.height = GConsts::PARTICLE_SIZE.y;

		Rectf coreBounds{GConsts::REACTOR_CORE_POSITION, GConsts::REACTOR_CORE_SIZE};
		handleParticleCollisions(coreBounds, particle, i / 4);
	}

	// Step velocities
	float speed = 150.0f;
	for (uint64 i = 0; i < m_particles.getVertexCount(); i += 4)
	{
		sf::Vertex *v = &m_particles[i];

		for (uint64 j = 0; j < 4; ++j)
		{
			v[j].position += speed * GET_APP()->getDeltaTime() * m_velocities[i / 4];
		}
	}
}

void ReactorParticles::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
	states.texture = m_texture;
	target.draw(m_particles, states);
}

void ReactorParticles::resetParticles()
{
	bool validSpawn = false;
	// TODO: Concurrency for particle spawning
	// const uint32 processorCount = std::thread::hardware_concurrency();
	// std::vector<std::thread> particleSpawnThreads;
	// particleSpawnThreads.resize(processorCount);
	Rectf particleRect;
	particleRect.width = GConsts::PARTICLE_SIZE.x;
	particleRect.height = GConsts::PARTICLE_SIZE.y;
	for (uint64 i = 0; i < m_particles.getVertexCount(); i += 4)
	{
		do
		{
			particleRect.left = Maths::RandFloat(GConsts::REACTOR_CORE_POSITION.x,
												 (GConsts::REACTOR_CORE_POSITION.x + GConsts::REACTOR_CORE_SIZE.x) - particleRect.width);

			particleRect.top = Maths::RandFloat(GConsts::REACTOR_CORE_POSITION.y,
												(GConsts::REACTOR_CORE_POSITION.y + GConsts::REACTOR_CORE_SIZE.y) - particleRect.height);

			for (auto &rect : m_fuelRods)
			{
				validSpawn = !rect.intersects(particleRect);
				if (!validSpawn)
				{
					break;
				}
			}

			if (!validSpawn)
			{
				continue;
			}

			for (auto &cr : m_controlRods)
			{
				// Rectf rect;
				// rect.left = cr->m_pTransform->getPosition().x;
				// rect.top = cr->m_pTransform->getPosition().y;
				// rect.width = GConsts::CONTROL_ROD_SIZE.x;
				// rect.height = GConsts::CONTROL_ROD_SIZE.y;

				validSpawn = !cr->getComponent<KCSprite>()->getOnscreenBounds().intersects(particleRect);
				if (!validSpawn)
				{
					break;
				}
			}

			if (!validSpawn)
			{
				continue;
			}

			spdlog::debug("Is valid spawn {} ", validSpawn);

			if (validSpawn)
			{
				sf::Vertex *v = &m_particles[i];
				Vec2f pos{particleRect.left, particleRect.top};
				v[0].position = pos;
				v[1].position = pos + Vec2f(particleRect.width, 0.0f);
				v[2].position = pos + Vec2f(particleRect.width, particleRect.height);
				v[3].position = pos + Vec2f(0.0f, particleRect.height);
			}

		} while (!validSpawn);

		const float randAngle = Maths::RandFloat(0, 359.0f);
		m_velocities[i / 4].x = cos(Maths::Radians(randAngle));
		m_velocities[i / 4].y = sin(Maths::Radians(randAngle));
	}
}

void ReactorParticles::handleParticleCollisions(const Rectf &object, const Rectf &particle, uint64 idx)
{
	if (particle.left < object.left ||
		particle.left + particle.width > object.left + object.width)
	{
		m_velocities[idx].x *= -1.0f; // Vec2f{m_velocities[idx].x * -1.0f, m_velocities[idx].y};
	}

	if (particle.top < object.top ||
		particle.top + particle.height > object.top + object.height)
	{
		m_velocities[idx].y *= -1.0f; //Vec2f{m_velocities[idx].x, m_velocities[idx].y * -1.0f};
	}

	m_velocities[idx] = Normalise(m_velocities[idx]);
}
