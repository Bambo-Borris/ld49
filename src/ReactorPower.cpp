#include "stdafx.h"
#include "ReactorPower.h"
#include "Constants.h"
#include "ReactorCore.h"

using namespace Krawler;
using namespace Krawler::Components;
using namespace Krawler::Input;

ReactorPower::ReactorPower(KEntity *entity)
	: LongFillPanel(entity, "Core Power")
{
}

KInitStatus ReactorPower::init()
{
	getEntity()->setTag(GConsts::REACTOR_POWER_TAG);
	getEntity()->m_pTransform->setPosition(GConsts::REACTOR_POWER_POSITION);

	auto result = LongFillPanel::init();
	if (result != KInitStatus::Success)
	{
		return result;
	}

	setFillColour(sf::Color::Green);

	return KInitStatus::Success;
}

void ReactorPower::tick()
{
	LongFillPanel::tick();
	
	auto coreEntity = GET_SCENE()->findEntity(GConsts::REACTOR_CORE_TAG);
	KCHECK(coreEntity);

	ReactorCore *core = coreEntity->getComponent<ReactorCore>();
	const float power = core->getPower();
	setFillPercentage((power / GConsts::REACTOR_MAX_POWER) * 100.0f);
}
