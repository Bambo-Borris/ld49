#include "stdafx.h"
#include "ReactorTemperature.h"
#include "Constants.h"
#include "ReactorCore.h"

#include <Components\KCSprite.h>

using namespace Krawler;
using namespace Krawler::Components;
using namespace Krawler::Input;

ReactorTemperature::ReactorTemperature(KEntity *entity)
	: LongFillPanel(entity, "Core Temperature")
{
}

KInitStatus ReactorTemperature::init()
{
	getEntity()->setTag(GConsts::REACTOR_TEMPERATURE_TAG);
	getEntity()->m_pTransform->setPosition(GConsts::REACTOR_TEMPERATURE_POSITION);

	auto result = LongFillPanel::init();
	if (result != KInitStatus::Success)
	{
		return result;
	}
	setFillColour(sf::Color::Red);

	return KInitStatus::Success;
}

void ReactorTemperature::tick()
{
	LongFillPanel::tick();

	auto coreEntity = GET_SCENE()->findEntity(GConsts::REACTOR_CORE_TAG);
	KCHECK(coreEntity);

	ReactorCore *core = coreEntity->getComponent<ReactorCore>();
	const float temp = core->getTemperature();
	setFillPercentage((temp / GConsts::REACTOR_MAX_TEMP) * 100.0f);
}
