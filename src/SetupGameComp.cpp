#include "stdafx.h"
#include "SetupGameComp.h"

#include "Constants.h"
#include "GameManagerComp.h"
#include "ReactorCore.h"
#include "ReactorTemperature.h"
#include "ReactorPower.h"
#include "ImguiComp.h"
#include "FuelRod.h"
#include "ControlRod.h"
#include "GameOverlay.h"
#include "ReactorParticles.h"

using namespace Krawler;

SetupGameComp::SetupGameComp(Krawler::KEntity *entity)
	: KComponentBase(entity)
{
}

KInitStatus SetupGameComp::init()
{
	GET_APP()->getRenderer()->setSortType(Renderer::KRenderSortType::LayerSort);
	const Vec2f SizeAsFloat(KCAST(float, GConsts::TARGET_WIDTH), KCAST(float, GConsts::TARGET_HEIGHT));

	auto scene = GET_DIRECTOR().getSceneByName(GConsts::GAMEPLAY_SCENE_NAME);
	KCHECK(scene);

	auto entity = getEntity();
	entity->setTag(GConsts::GAME_MANAGER_TAG);

	auto sprite = new Components::KCSprite(entity, SizeAsFloat);
	KCHECK(sprite);
	auto tex = ASSET().getTexture("screen_outline");
	KCHECK(tex);
	// sprite->setColour(sf::Color{100, 100, 100});
	sprite->setRenderLayer(-1);
	sprite->setTexture(tex);
	entity->addComponent(sprite);

	auto dbgimgui = new ImguiComp(entity);
	entity->addComponent(dbgimgui);

	spdlog::set_default_logger(GET_APP()->m_logger);
	spdlog::set_level(spdlog::level::level_enum::debug);

	// Set view to our target view
	auto renderWindow = GET_APP()->getRenderWindow();
	auto currentView = renderWindow->getView();
	currentView.setSize(SizeAsFloat);
	currentView.setCenter(SizeAsFloat * 0.5f);
	renderWindow->setView(currentView);

	entity->addComponent(new GameManagerComp(entity));

	// Add reactor stuff
	auto reactorCore = scene->addEntityToScene();
	reactorCore->addComponent(new ReactorCore(reactorCore));

	// Add reactor temperature stuff
	auto reactorTemp = scene->addEntityToScene();
	reactorTemp->addComponent(new ReactorTemperature(reactorTemp));

	// Add reactor temperature stuff
	auto reactorPower = scene->addEntityToScene();
	reactorPower->addComponent(new ReactorPower(reactorPower));

	// Add fuel rods
	std::vector<KEntity *> fuelRods;
	bool didCreate = scene->addMultipleEntitiesToScene(GConsts::FUEL_ROD_COUNT, fuelRods);
	KCHECK(didCreate);

	// Below is some super jank code for setting the fuel rods to be centred inside the reactor core
	// It creates a bounding box around the fuel rodss, calculates its top left then sets the first fuel rod
	// position to this top left. Then every other fuel rod has its transform parented to the first fuel rods
	// and then we appy padding of (FUEL_ROD_WIDTH + SPACING) * FUEL_ROD_INDEX
	const float BetweenRodPadding = 36.0f;
	const Vec2f CentrePos = GConsts::REACTOR_CORE_POSITION + GConsts::REACTOR_CORE_SIZE * 0.5f;
	Vec2f size((GConsts::FUEL_ROD_COUNT * GConsts::FUEL_ROD_SIZE.x) + (GConsts::FUEL_ROD_COUNT * BetweenRodPadding), GConsts::FUEL_ROD_SIZE.y);
	Vec2f firstFuelRodPos = (CentrePos - size * 0.5f);

	for (uint64 i = 0; i < fuelRods.size(); ++i)
	{
		KCHECK(fuelRods[i]);

		fuelRods[i]->addComponent(new FuelRod(fuelRods[i]));
		fuelRods[i]->setTag(fmt::format("{}{}", GConsts::FUEL_ROD_TAG, i));

		if (i != 0)
		{
			fuelRods[i]->m_pTransform->setParent(fuelRods[0]);
			fuelRods[i]->m_pTransform->setPosition((GConsts::FUEL_ROD_SIZE.x + BetweenRodPadding) * KCAST(float, i), 0.0f);
		}
		else
		{
			fuelRods[i]->m_pTransform->setPosition(firstFuelRodPos.x + BetweenRodPadding / 2.0f, firstFuelRodPos.y);
		}
	}

	// Add control rods
	std::vector<KEntity *> controlRods;
	didCreate = scene->addMultipleEntitiesToScene(GConsts::CONTROL_ROD_COUNT, controlRods);
	KCHECK(didCreate);

	for (uint64 i = 0; i < controlRods.size(); ++i)
	{
		KCHECK(controlRods[i]);
		spdlog::debug("Fuel Rod Postion => {} {}", fuelRods[i]->m_pTransform->getPosition().x, fuelRods[i]->m_pTransform->getPosition().y);
		controlRods[i]->addComponent(new ControlRod(controlRods[i]));
		Vec2f rodPosition;
		rodPosition.x = fuelRods[i]->m_pTransform->getPosition().x + GConsts::CONTROL_ROD_MAX_SIZE.x +
						(BetweenRodPadding / 2.0f) + (GConsts::CONTROL_ROD_SIZE.x / 2.0f);
		rodPosition.y = GConsts::REACTOR_CORE_POSITION.y;

		controlRods[i]->m_pTransform->setPosition(rodPosition);
	}

	// Add an overlay entity (used to dim whole game screen during death/initial play)
	auto overlay = scene->addEntityToScene();
	KCHECK(overlay);
	overlay->setTag(GConsts::OVERLAY_TAG);
	auto overlayComp = new GameOverlay(overlay);
	overlay->addComponent(overlayComp);
	overlay->setActive(false);

	// Arrows for range
	auto lowerTex = ASSET().getTexture("lower_bound"), upperTex = ASSET().getTexture("upper_bound");
	KCHECK(lowerTex);
	KCHECK(upperTex);

	if (!lowerTex || !upperTex)
	{
		return KInitStatus::MissingResource;
	}

	auto lowerBound = scene->addEntityToScene();
	KCHECK(lowerBound);
	Components::KCSprite *lowerBoundSprite = new Components::KCSprite(lowerBound);
	lowerBoundSprite->setSize(Vec2f(lowerTex->getSize()));
	lowerBoundSprite->setRenderLayer(3);
	lowerBoundSprite->setTexture(lowerTex);
	lowerBound->addComponent(lowerBoundSprite);
	lowerBound->setTag(GConsts::LOWER_BOUND_TAG);
	lowerBound->m_pTransform->setOrigin(0.0f, KCAST(float, lowerTex->getSize().y) / 2.0f);
	lowerBound->m_pTransform->setScale(0.5f, 0.5f);

	auto upperBound = scene->addEntityToScene();
	KCHECK(upperBound);
	Components::KCSprite *upperBoundSprite = new Components::KCSprite(upperBound);
	upperBoundSprite->setSize(Vec2f(upperTex->getSize()));
	upperBoundSprite->setRenderLayer(3);
	upperBoundSprite->setTexture(upperTex);
	upperBound->addComponent(upperBoundSprite);
	upperBound->setTag(GConsts::UPPER_BOUND_TAG);
	upperBound->m_pTransform->setOrigin(0.0f, KCAST(float, upperTex->getSize().y) / 2.0f);
	upperBound->m_pTransform->setScale(0.5f, 0.5f);

	auto reactorParticles = scene->addEntityToScene();
	KCHECK(reactorParticles);
	reactorParticles->addComponent(new ReactorParticles(reactorParticles));

	return KInitStatus::Success;
}
